<?php

/**
 * @file
 * Meneame module settings file.
 *
 * Meneame is a tweeting service. This module allows the submission of Drupal
 * content to Meneame services, adding a button to add a vote in the Meneame
 * views.
 *
 */

/**
 * Meneame settings form.
 *
 * Build a form to select the content types to be submitted to Meneame, and the
 * view types where the button must be shown.
 */
function meneame_node_settings() {
  $form = array();

  $form['meneame_node_types'] = array(
    '#type'           => 'checkboxes',
    '#title'          => t('Node Types'),
    '#options'        => node_get_types('names'),
    '#default_value'  => variable_get('meneame_node_types', array('story')),
  );

  $location_options = array(
    'full' => t('Full View'),
    'teaser' => t('Teasers'),
  );
  $form['meneame_node_location'] = array(
    '#type'           => 'checkboxes',
    '#title'          => t('Locations'),
    '#options'        => $location_options,
    '#default_value'  => variable_get('meneame_node_location', array('full')),
  );


  $form['meneame_node_weight'] = array(
    '#type'           => 'weight',
    '#title'          => t('Weight'),
    '#default_value'  => variable_get('meneame_node_weight', -5),
    '#description'    => t('Heavier weight will sink button to bottom on node view.'),
  );

  return system_settings_form($form);
}
